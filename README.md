# stm

A home-built scanning tunneling microscope controlled by an ESP32 board

## Short description 
A scanning tunneling microscope features a very sharp tip that moves just above the surface of a (semi)conductive sample to determine its shape, using electron tunneling effect to keep the sub-nanometre tip−sample distance constant. In good conditions, an STM can image individual atoms.

There are several more-or-less successful amateur STM projects online ([J. Gatt](http://www.angelfire.com/electronic2/spm/), [M. Logusz](https://michaellogusz.blogspot.com/2017/10/scanning-tunneling-microscope-work-in.html), [S. De'Angeli](http://hackedgadgets.com/2010/09/23/chemhacker-scanning-tunneling-electron-microscope-schematics-and-source-released/comment-page-1/), [J. Müller](http://www.e-basteln.de/other/stm/overview/), [D. Berard](http://dberard.com) etc.). Existing overviews (e. g. [J. Logajan](http://www.nanotech-now.com/James-Logajan/stm.htm)) list another dead links from 90's and 00's. 
This is an attempt to put STM design into a truly open-sourced and futureproof [git](http://git-scm.org) repository. Aside of this, it should improve over the existing designs to make routine measurements more comfortable: the welded frame provides enough room for easy sample replacement and micro-positioning, an up-to-date digital controller [ESP32](https://docs.zerynth.com/latest/official/board.zerynth.doit_esp32/docs/index.html) is an affordable and flexible replacement for most analog electronics, and the multiplatform graphical user interface written in Python+Matplotlib enables convenient operation and direct export to [gwyddion](http://gwyddion.net). Still, the project is kept at reasonably "low-tech" level so that anybody with spare $300 and 50 hours of free time, some skills in electronics and access to a mechanical workshop should be able to build their own instrument. 

The following image shows my second prototype, with the frame welded from 30×30mm iron tubes, a digital control circuit on the breadboard and an oscilloscope revealing the sample profiles (red curve) during scanning. 

![STM setup overview photo](example_images/stm_setup_crop.jpg)

As of late 2019, the prototype can acquire images with the resolution of tens of nanometres (see below). I am occasionally improving the firmware and computer software, as well as the tip etching procedure, with the hope that noise and artifacts will be reduced and resolution improves down to low-nanometre level. 



## Example results  (to be expanded)
One of the first testing images acquired showing the grooves on the silvered surface of a broken DVD-R. The scanned area was roughly 1.7×1.7 nm with quadrant voltage sweeping over ca. 5 V range (out of ≈15 V available for each axis). The X and Y axes were calibrated against the known groove spacing (740 nm), whereas the vertical Z-axis was calibrated using the Z-table micrometric screw. The image has been edited in gwyddion (row alignment, scar removal, levelling).
![Example image](example_images/stm_dvd.png)

## Technical details
#### Mechanical assembly  (to be added)
image of fundamental components, making the piezo scanner, casting concrete or welding the frame?, electronics, buying or making the sample table?
#### Making your own tungsten tips  (to be added)
#### Electrical circuitry  (to be added)
choice of ESP32 over other boards, 
driving the DAC using I2S (and choice of TDA1543), 
Stepstick stepper controller
tunelling current pre-amplifier

#### How firmware and software cooperate to acquire an image  (to be added)

## Making it work step-by-step  (to be added)
 1.  does the controller board receive program from Arduino? 
 1.  * If your Linux machine complains about ```PermissionError: [Errno 13] Permission denied: '/dev/ttyUSB0'```, run ```sudo adduser $USER dialout``` and reboot
 1.  does the controller board connect to the STM software in the computer? 

 1.  does the controller board drive the DAC? 
 1.  does the DAC emit analog signal?
 1.  does the piezo amplifier give correct waveforms?
 1.  does the tunelling current pre-amplifier sense tip voltage?

 1.  does the piezo feedback work?

 1.  does the controller board drive the StepStick? 
 1.  does StepStick drive motors and the sample table moves in correct direction?

 1.  does the rough approach stop exactly the tip first touches the sample?
 1.  does the piezo feedback maintain stable tunelling current without mechano-electrical oscillations?
 1.  does the microscope scan sample surface in a repeatable manner? 

 1.  does the STM acquire an image with a good tradeoff of noise and speed? Is the tip sharp?

An example of a semiconductor sample imaged with a tip that was blunt and probably covered with WO₃, introducing convolution and random scars:

![Blunt tip artifacts](example_images/blunt_tip_artifacts_3D.png)

## Practical tricks for day-to-day operation  (to be added)

## Important TODOs 
    * switch to 32bit piezo positioning
    * allow run-time setting of feedback constants from GUI
    * new message #0 called IDENTIFY, which returns a type (and a serial number?) of the hardware
    * on serial.serialutil.SerialException: 

    * device autodetection on serial Windows
    write failed: [Errno 5] Input/output error re-connect to a different port (seek for a STM signature, todo for C code!)

## Nice-to-have TODOs 
    * 4x "sigma-delta" subsampling on DAC 
    * draw nice illustrations for this README
    * test on native Windows
    * end switch for Z-table
    * checking for sample grounding
    * current-voltage and current-height curve measurement
    * clearer code: shared defs (e.g. message formats) in a common .h file & parse it with python!
    * 3D imaging  with:

    ax = plt.axes(projection='3d'); ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='winter', edgecolor='none')  #TODO


