#include "driver/i2s.h"
/*
urgent TODO:
	check duty cycle of loop code sections
	
 */

// == APPROACH AND SCANNING CONTROL == 

uint8_t approach_active = 0;
uint8_t approach_stepper_number;    
int32_t approach_motor_targetpos;   
int32_t approach_motor_speed;       
int32_t approach_motor_cycle_steps; 
int16_t approach_piezo_speed;       

int16_t scan_x_start = 0;       
int16_t scan_x_end = 0;       
int16_t scan_x_speed = 0;       
int16_t scan_x_sampling_step = 100;       
int16_t scan_y_position = 0;       

uint8_t piezo_feedback_active = 0;
uint8_t piezo_scan_active = 0;

//#define SUBSAMPLE_BITS    8		//  TODO this is piezo control should allow for slower scanning than 1 LSB per cycle
#define ADC_THRESHOLD    1000	// feedback from the 12bit built-in ADC: full value of 4095 corresponds to ca. 3.3 V

// == HARDWARE CONTROL PINS AND VARIABLES == 

// Following code allows for flexible control of up to 256 stepper motors. Just add a new column to the following arrays. 
// Note: using "nanopos" and "nanospeed" are essential for stepping speeds less than one microstep per cycle.

#define STEPPER_COUNT   1
                                       //  motor1  ...
uint8_t stepper_step[STEPPER_COUNT]      = {23};
uint8_t stepper_dir[STEPPER_COUNT]       = {22};
uint8_t stepper_off[STEPPER_COUNT]       = {15};
uint8_t stepper_end[STEPPER_COUNT]       = {13}; 

#define NANOSTEP_PER_MICROSTEP  256  // maximum main loop cycles per step (higher value enables finer control of speed, but smaller range)
#define MOTOR_INERTIA_COEF 200		 // smooth ramp-up and ramp-down of motor speed 
int32_t nanopos[STEPPER_COUNT]           = {0};	// current motor position 
int32_t target_nanopos[STEPPER_COUNT]    = {0}; // set from the computer; (zero usually corresponds to the lower end switch)
int32_t nanospeed[STEPPER_COUNT]         = {1}; // set from the computer; always ensure that 0 < nanospeed < NANOSTEP_PER_MICROSTEP 

int32_t previous_nanopos[STEPPER_COUNT]   = {0}; // used for smooth speed control
uint8_t auto_motor_turnoff[STEPPER_COUNT] = {1}; // if set to 1, the motor "disable" pin will be set when not moving
uint8_t was_at_end_switch[STEPPER_COUNT] = {0};

#define LED_BUILTIN   2		// visual feedback

#define DAC_CLOCK     5		// common signals to digital-analog converters (DAC)
#define DAC_LATCHEN  18
#define DAC_DATA0    19		// synchronous data outputs to four DACs
#define DAC_DATA1    21
#define DAC_DATA2    14
#define DAC_DATA3    27

#define NDAC_BCK       4      // testing the preferred dual 16-bit TDA1543
#define NDAC_WS		   12
#define NDAC_DATA01    26		// first 2 channels
#define NDAC_DATA23    25		// next 2 channels


int16_t x = 0;		// Cartesian coords will be linearly combined for the four-quadrant piezo transducer
int16_t y = 0;
int16_t z = 0;

// Piezo controlling pseudo-constants
#define ADC_PIN          33				// input from the STM tip pre-amplifier


// == COMPUTER COMMUNICATION == 


#define IN_BUF_LEN 40
uint8_t in_buf[IN_BUF_LEN];     // input message buffer, first byte always indicates the type of message (and its expected length)
uint8_t in_buf_ptr = 0;         // pointer behind the last received byte

//#define MESSAGE_TIMEOUT_LIMIT 100 // TODO implement fail-safe 
//uint8_t in_buf_timeout = 0; // erase the input buffer if more than MESSAGE_TIMEOUT_LIMIT main loop cycles pass without receiving next byte 

#define OUT_BUF_LEN 4000
uint8_t out_buf[OUT_BUF_LEN];		// output message buffer
int16_t out_buf_ptr = 0;			// pointer to buffer end
uint8_t stm_data[OUT_BUF_LEN];   // raw data recorded by the STM scanning routine
int16_t stm_data_ptr = 0;

#define CMD_STEPPERM  1		// go with the stepper motor to a given position
struct cmd_stepperm_struct  {  
    uint8_t message_type;      // always byte 0 
    uint8_t stepper_number;    // at byte 1
    int32_t motor_targetpos;   // at byte 2
    int32_t motor_speed;       // at byte 6
} __attribute__((packed));     // (prevent byte padding of uint8 and uint16 to four bytes)
#define CMD_APPROACH 2		// safely approach the sample to the STM tip, using both stepper and piezo
struct cmd_approach_struct  {  
    uint8_t message_type;                
    uint8_t stepper_number;    // at byte 1
    int32_t motor_targetpos;   // at byte 2
    int32_t motor_speed;       // at byte 6
    int32_t motor_cycle_steps; // at byte 10
    int16_t piezo_speed;       // at byte 14
} __attribute__((packed)); 
#define CMD_GET_STEPPER_STATUS 3		// just report the current nanopos and status
struct cmd_get_stepper_status_struct  {  
    uint8_t message_type;                
    uint8_t stepper_number;    // at byte 1
} __attribute__((packed)); 
#define CMD_GET_STM_STATUS 4		// just report the current nanopos and status
struct cmd_get_stm_status_struct  {  
    uint8_t message_type;                
	// TODO
} __attribute__((packed)); 
#define CMD_SET_PIEZO  9		// set a concrete position on the piezo
struct cmd_set_piezo_struct  {  
    uint8_t message_type;      // always byte 0
    int16_t piezo_x;			   // at byte 1			(note that full uint32 range may not be used by the dac)
    int16_t piezo_y;			   // at byte 5			
    int16_t piezo_z;			   // at byte 9		
} __attribute__((packed));
#define CMD_LINESCAN 10
struct cmd_linescan_struct  {  // todo
    uint8_t message_type;                
	int16_t scan_x_start;			   // at byte  1
	int16_t scan_x_end;                // at byte  5
	int16_t scan_x_speed;              // at byte  9
	int16_t scan_x_sampling_step;      // at byte 13
	int16_t scan_y_position;           // at byte 17
} __attribute__((packed)); 

#define DACBITS 16						// dac bit depth
#define bitmask		(1<<(DACBITS-1))
#define dac_value_shift (128*256)
	//(1<<(DACBITS-1)) FIXME
#define dummyb_max	7
#define bit_max		DACBITS
void set_piezo_raw(int16_t dac_word0, int16_t dac_word1, int16_t dac_word2, int16_t dac_word3) {
// Communication with 2x TDA1543 dual 16-bit DACs, using I2S protocol implemented in firmware. This takes 20-25 us.
//
// Note 0: Here we are not using the built-in I2S interface of ESP32 since we want to supply data to multiple DACs at once.
// Note 1: By much trial and error, I learned that the TDA1543 chips require the most significant bit 
//   extending to exactly (!) 7+1 leading bits of the signal. In fact we are loading 23 bits that contain the 16bit 
//   number at their end. Otherwise the input data got truncated.
// Note 2: The digitalWrite routine takes ca. 60 nanoseconds by default, which is just enough for the fastest 
//   timing delays from the TDA1543 datasheet. Thanks to digitalWrite() being rather inefficient, no waiting routines 
//   are needed, unless one overclocks the ESP32 or uses direct port access.
// Note 3: From the nature of the I2S protocol, the DAC channels 0 and 1 might be expected to be out-of-sync if 
//   if the Left and Right channels are set up incorrectly (by a single 100 us cycle). Currently the actual output 
//   signal of both DAC channels looks to be perfectly in sync anyway.

	digitalWrite(NDAC_WS, HIGH);
	digitalWrite(NDAC_BCK, LOW);
	for (uint8_t bit=0; bit<bit_max; bit++) {
		if (dac_word0 & bitmask) {digitalWrite(NDAC_DATA01, HIGH);} else {digitalWrite(NDAC_DATA01, LOW);};  dac_word0 = dac_word0 << 1; 
		if (dac_word2 & bitmask) {digitalWrite(NDAC_DATA23, HIGH);} else {digitalWrite(NDAC_DATA23, LOW);};  dac_word2 = dac_word2 << 1;
		if (bit==0) {
			for (uint8_t dummyb=0; dummyb<dummyb_max; dummyb++) {
			digitalWrite(NDAC_BCK, HIGH); // the digitalWrite routine takes ca. 60 ns in default ESP32 - this is crucial for timing
			digitalWrite(NDAC_BCK, LOW);
			}
		}
		digitalWrite(NDAC_BCK, HIGH);
		digitalWrite(NDAC_BCK, LOW);
		if (bit==(bit_max-2)) { digitalWrite(NDAC_WS, LOW);	}	
	}
	for (uint8_t bit=0; bit<bit_max; bit++) {
		if (dac_word1 & bitmask) {digitalWrite(NDAC_DATA01, HIGH);} else {digitalWrite(NDAC_DATA01, LOW);};  dac_word1 = dac_word1 << 1;
		if (dac_word3 & bitmask) {digitalWrite(NDAC_DATA23, HIGH);} else {digitalWrite(NDAC_DATA23, LOW);};  dac_word3 = dac_word3 << 1;
		if (bit==0) {
			for (uint8_t dummyb=0; dummyb<dummyb_max; dummyb++) {
			digitalWrite(NDAC_BCK, HIGH);
			digitalWrite(NDAC_BCK, LOW);
			}
		}
		digitalWrite(NDAC_BCK, HIGH);
		digitalWrite(NDAC_BCK, LOW);
		if (bit==(bit_max-2)) { digitalWrite(NDAC_WS, HIGH);	}	
	}
}

#define HARD_LIMIT_LOW  -(1<<(DACBITS-1))						// "Hard" limits prevent DAC over- and under- flows. Should never be exceeded.
#define HARD_LIMIT_HIGH (1<<(DACBITS-1))	
#define HARD_LIMIT_CENTRE (HARD_LIMIT_LOW+HARD_LIMIT_HIGH)/2
#define HARD_LIMIT_EXTENT (HARD_LIMIT_HIGH-HARD_LIMIT_LOW)
#define SOFT_LIMIT_LOW    (HARD_LIMIT_CENTRE-HARD_LIMIT_EXTENT/6+1)  // "Soft" limits prevent STM range clipping during Cartesian-quadrant conv.
#define SOFT_LIMIT_HIGH   (HARD_LIMIT_CENTRE+HARD_LIMIT_EXTENT/6-1)  // Note: if e.g. x=y=0, the z coord can go up to hard limits without trouble
//void set_piezo(int16_t target_x, int16_t target_y, int16_t target_z) {
	//set_piezo_slow(target_x, target_y, target_z, 0);
//}


void set_piezo_slow(int16_t target_x, int16_t target_y, int16_t target_z, int16_t speed_limit) { 
  while ((x!=target_x) || (y!=target_y) || (z!= target_z)) { 
	if (speed_limit == 0) { 
		x=target_x; y=target_y; z= target_z; 
	} else {
		if (x > target_x) {x = max(target_x, x-speed_limit);}
		if (x < target_x) {x = min(target_x, x+speed_limit);}

		if (y > target_y) {y = max(target_y, y-speed_limit);}
		if (y < target_y) {y = min(target_y, y+speed_limit);}

		if (z > target_z) {z = max(target_z, z-speed_limit);}
		if (z < target_z) {z = min(target_z, z+speed_limit);}
		ets_delay_us(100); 
    }

	set_piezo_raw(+ x + y + z,
                  - x + y + z,
                  - x - y + z,
                  + x - y + z); 
  }				 
}

void transmit_out_buf(int32_t total_bytes) {
  for (out_buf_ptr=0; out_buf_ptr<total_bytes; out_buf_ptr++) { Serial.write(out_buf[out_buf_ptr]); }
}

void process_messages() {
    if ((in_buf[0] == CMD_STEPPERM) && (in_buf_ptr == sizeof(cmd_stepperm_struct))) {
		digitalWrite(LED_BUILTIN, HIGH); ets_delay_us(10000); digitalWrite(LED_BUILTIN, LOW); 
        uint8_t m                = in_buf[1];                // identify the motor
		if (m<STEPPER_COUNT) {
			previous_nanopos[m]      = nanopos[m]; // remember the starting position (for speed control)
			target_nanopos[m]        = *((int32_t*)(in_buf+ 2));  
			nanospeed[m]             = *((int32_t*)(in_buf+ 6)); 
			approach_active  = 0;
		}
        in_buf_ptr = 0;
    } else if ((in_buf[0] == CMD_APPROACH) && (in_buf_ptr == sizeof(cmd_approach_struct))) {
		digitalWrite(LED_BUILTIN, HIGH); ets_delay_us(10000); digitalWrite(LED_BUILTIN, LOW); 
        set_piezo_slow(0,0,SOFT_LIMIT_LOW, 100); // fast retract piezo to centre top 

		approach_stepper_number     = *((uint8_t*)(in_buf+ 1)); // at byte 1
		approach_motor_targetpos    = *((int32_t*)(in_buf+ 2)); // at byte 2
		approach_motor_speed        = *((int32_t*)(in_buf+ 6)); // at byte 6
		approach_motor_cycle_steps  = *((int32_t*)(in_buf+10)); // at byte 10
		approach_piezo_speed        = *((int16_t*)(in_buf+14)); // at byte 14

		piezo_feedback_active = 0;
		piezo_scan_active = 0;
		approach_active  = 1;

        in_buf_ptr = 0;

    } else if ((in_buf[0] == CMD_GET_STEPPER_STATUS) && (in_buf_ptr == sizeof(cmd_get_stepper_status_struct))) {
		uint8_t m = in_buf[1];
		if (m<STEPPER_COUNT) {
			if (nanopos[m] == target_nanopos[m]) {out_buf[0] = 0;} else {out_buf[0] = 1;}; 
			if (!digitalRead(stepper_end[m])) {out_buf[1] = 0;} else {out_buf[1] = 1;}; 
			memcpy(out_buf+2, &nanopos[m], sizeof(nanopos[m])); out_buf_ptr += sizeof(nanopos[m]);
			transmit_out_buf(6);
		}
        in_buf_ptr = 0;
    } else if ((in_buf[0] == CMD_GET_STM_STATUS) && (in_buf_ptr == sizeof(cmd_get_stm_status_struct))) {
		if (approach_active) {out_buf[0] = (uint8_t)1;} else {out_buf[0] = 0;}; 
		if (piezo_feedback_active) {out_buf[1] = (uint8_t)1;} else {out_buf[1] = 0;}; 
		if (piezo_scan_active) {out_buf[1] += (uint8_t)2;}; 
		memcpy(out_buf+2,  &x, 2);
		memcpy(out_buf+4,  &y, 2);
		memcpy(out_buf+6, &z, 2);
		int16_t adcvalue = analogRead(ADC_PIN);
		memcpy(out_buf+8, &adcvalue, 2);
		memcpy(out_buf+10, &stm_data_ptr, 2);
		for (out_buf_ptr=0; out_buf_ptr<12;  out_buf_ptr++)  { Serial.write(out_buf[out_buf_ptr]); } // todo use transm...
		//transmit_out_buf(12);
		//for (int16_t i=0; i<stm_data_ptr; i++) { Serial.write(123); }
		for (int16_t i=0; i<stm_data_ptr; i++) { Serial.write(stm_data[i]); }
		stm_data_ptr = 0;
        in_buf_ptr = 0;
    } else if ((in_buf[0] == CMD_SET_PIEZO) && (in_buf_ptr == sizeof(cmd_set_piezo_struct))) {
		//set_piezo_slow(*((int16_t*)(in_buf+ 2)), *((int16_t*)(in_buf+ 4)), *((int16_t*)(in_buf+ 8)), 100); 
        in_buf_ptr = 0;
		set_piezo_slow(x, y, SOFT_LIMIT_LOW, 100); 
		set_piezo_slow(*((int16_t*)(in_buf+ 1)), *((int16_t*)(in_buf+ 3)), SOFT_LIMIT_LOW, 100); 
    } else if ((in_buf[0] == CMD_LINESCAN) && (in_buf_ptr == sizeof(cmd_linescan_struct))) {
		scan_x_start		= *((int16_t*)(in_buf+ 1)); 
		scan_x_end        = *((int16_t*)(in_buf+ 3));
		scan_x_speed      = *((int16_t*)(in_buf+ 5));
		scan_x_sampling_step  = *((int16_t*)(in_buf+7));
		scan_y_position = *((int16_t*)(in_buf+9)); 

		// safely go to the initial piezo position and enable scanning (in the main routine)
		piezo_feedback_active = 1;
		piezo_scan_active = 1;

        in_buf_ptr = 0;

		//out_buf[0] = x;
		//out_buf[1] = y;
		//for (out_buf_ptr=0; out_buf_ptr<5; out_buf_ptr++) {
			//Serial.write(out_buf[out_buf_ptr]);
		//}
		//target_nanopos[0] = *((uint32_t*)(buffer+1));     // how to receive an uint32
		//uint32_t tmp =  42*256*256+256+123;				// how to send an uint32
		//memcpy(out_buf+out_buf_ptr, &tmp, sizeof(tmp)); out_buf_ptr += sizeof(tmp);

	};
};


void setup() {/*{{{*/
  Serial.begin(921600);

  // stepper motor control
  for (uint8_t m=0; m<STEPPER_COUNT; m++) {  
	  pinMode(stepper_step[m], OUTPUT);
	  pinMode(stepper_dir[m],  OUTPUT);
	  digitalWrite(stepper_off[m],  HIGH);
	  pinMode(stepper_off[m],  OUTPUT);
	  pinMode(stepper_end[m],  INPUT_PULLUP);
  }

  // New DAC pins (TDA1543)
  pinMode(NDAC_BCK   , OUTPUT);   // clock is common for all DACs
  pinMode(NDAC_WS    , OUTPUT);
  pinMode(NDAC_DATA01, OUTPUT);
  pinMode(NDAC_DATA23, OUTPUT);

  // other pins
  pinMode(LED_BUILTIN,  OUTPUT);
}/*}}}*/


void loop() {
  while (Serial.available() && (in_buf_ptr<IN_BUF_LEN)) { 
      in_buf[in_buf_ptr] = Serial.read(); 
      in_buf_ptr += 1; 
      process_messages();
      //in_buf_timeout = 0; 
  }

  // Piezo control DEBUG XXX
  //int16_t q = 0;
  //while (1) { 
	//ets_delay_us(1);
	//int16_t tempstep = 10; // @500ms x1 doublerange CORRECT?
	//set_piezo_slow(0,0,q,  100);
	//if (q >= (SOFT_LIMIT_HIGH-tempstep)) {
		//q=SOFT_LIMIT_LOW;
	    //digitalWrite(LED_BUILTIN, HIGH);	digitalWrite(LED_BUILTIN, LOW);	
		//} else {q += tempstep; }; 
  //}



  // Motor control
  int32_t new_nanopos, limited_nanospeed;
  for (uint8_t m=0; m<STEPPER_COUNT; m++) {
    if (!digitalRead(stepper_end[m])) { nanospeed[m] = 32; target_nanopos[m] = nanopos[m]+nanospeed[m]+1; } // get out from lower end switch
    else if (was_at_end_switch[m]) { nanopos[m] = 0; target_nanopos[m] = 1; } // when freshly got from the end switch: calibrate position to zero
    was_at_end_switch[m] = !digitalRead(stepper_end[m]); // remember end stop state

	limited_nanospeed = min(nanospeed[m],      (abs(nanopos[m] - target_nanopos[m]))/MOTOR_INERTIA_COEF+1);
	limited_nanospeed = min(limited_nanospeed, (abs(nanopos[m] - previous_nanopos[m]))/MOTOR_INERTIA_COEF+1);

    if (stepper_off[m]) {
      if (nanopos[m] != target_nanopos[m]) { digitalWrite(stepper_off[m], LOW);} else { digitalWrite(stepper_off[m], HIGH);}
    }
    
    if (nanopos[m] < target_nanopos[m]) { 
    //digitalWrite(LED_BUILTIN, HIGH); 
      digitalWrite(stepper_dir[m], HIGH); 
      new_nanopos = min(nanopos[m] + limited_nanospeed, target_nanopos[m]);
    } else if (nanopos[m] > target_nanopos[m]) { 
    digitalWrite(LED_BUILTIN, HIGH); 
      digitalWrite(stepper_dir[m], LOW); 
      new_nanopos = max(nanopos[m] - limited_nanospeed, target_nanopos[m]);
    } else { 
    digitalWrite(LED_BUILTIN, LOW); 
      new_nanopos = nanopos[m];
    };

	if ((new_nanopos/NANOSTEP_PER_MICROSTEP) != (nanopos[m]/NANOSTEP_PER_MICROSTEP)) {digitalWrite(stepper_step[m], HIGH);}
    nanopos[m] = new_nanopos;
  }
  ets_delay_us(1);  
  for (uint8_t m=0; m<STEPPER_COUNT; m++) {digitalWrite(stepper_step[m], LOW);}  // falling edge of stepper pulse




  // Coarse approach strategy (motor + piezo)

  if (approach_active) {
      int16_t adcpin = (int16_t)analogRead(ADC_PIN);
	  if (adcpin > ADC_THRESHOLD/2) {  
		if (nanopos[approach_stepper_number] == target_nanopos[approach_stepper_number]) {
            if (z < (SOFT_LIMIT_HIGH-approach_piezo_speed)) {
			  // motor reached its temporary target position (without contact), move piezo now
			  set_piezo_slow(0, 0, z+approach_piezo_speed,   0); 
			} else {
			  // piezo reached its limit (without contact), retract piezo to the very lowest position, then approach with motor
			  ets_delay_us(1000);
			  set_piezo_slow(0, 0, SOFT_LIMIT_LOW, 100); 
			  ets_delay_us(1000);
			  nanospeed[approach_stepper_number] = approach_motor_speed;
			  if (nanopos[approach_stepper_number] < (approach_motor_targetpos-approach_motor_cycle_steps)) {
			  	target_nanopos[approach_stepper_number] += approach_motor_cycle_steps;
			  } else { 
			    // motor reaches target position without any contact, terminate the approach here without success:
			  	target_nanopos[approach_stepper_number] = approach_motor_targetpos;
			    approach_active = 0;
			  };
			};	
          };
		} else { // STM contact established
          if (z < (SOFT_LIMIT_HIGH-approach_piezo_speed)) {  // but don't get confused by noise when stepper motor runs
			  target_nanopos[approach_stepper_number] = nanopos[approach_stepper_number]; 
			  approach_motor_targetpos = nanopos[approach_stepper_number]; 
			  approach_active = 0;
			  piezo_feedback_active = 1;
          }
		}  
  }


  // STM vertical feedback and scanning control (piezo):  (tip voltage dropping => retract => lower piezo voltage)
  if (piezo_feedback_active) {
  int16_t new_x = x; // TODO: avoid hard crashes, i.e. stop x,y movement when adcpin is too low
    if (piezo_scan_active) {
      y = scan_y_position;       
      if (scan_x_speed > 0) {
		if (new_x < (scan_x_end-scan_x_speed)) {		// scanning right
			new_x += scan_x_speed;
		}
		else {		// rightmost turn
		  //piezo_scan_active = 0; // XXX
    	  scan_x_speed = 0-scan_x_speed;
          new_x += scan_x_speed;
		}
	  }
      else 
      {
		if (new_x > (scan_x_start+scan_x_speed)) 
        {	// scanning left
            new_x += scan_x_speed;
        }
        else 
        {	// leftmost end position
		  piezo_scan_active = 0;
        }
      }
    }
	//if (piezo_scan_active) { digitalWrite(LED_BUILTIN, HIGH);} else { digitalWrite(LED_BUILTIN, LOW); }
    int16_t adcpin = analogRead(ADC_PIN);

	// Still troubles with clever feedback:
	//set_piezo_slow(x, y, max(SOFT_LIMIT_LOW+approach_piezo_speed, min(SOFT_LIMIT_HIGH-approach_piezo_speed, z+(adcpin-ADC_THRESHOLD)/100)),   0); 

	// Separate response sensitivity for going up and down
	int16_t new_z;
	if (adcpin>ADC_THRESHOLD) {
		new_z = z+(adcpin-ADC_THRESHOLD)/20; // faster contacting
	} 
    if (adcpin<(ADC_THRESHOLD)) {
		new_z = z+(adcpin-ADC_THRESHOLD)/50; // stiffer retracting
	}
	if ((adcpin<(ADC_THRESHOLD-100)) || (adcpin>(ADC_THRESHOLD+100))) { 
		new_x = x; // pause x-scanning until current is OK
	}
    if (piezo_scan_active) {
      if ((x / scan_x_sampling_step) != (new_x / scan_x_sampling_step)) {
    	memcpy(stm_data+stm_data_ptr, &z, sizeof(z)); //stm_data[stm_data_ptr] = z; 
		if (stm_data_ptr<OUT_BUF_LEN/2) {stm_data_ptr+=sizeof(z);} 
      }
    }
    //if (piezo_scan_active) {
	set_piezo_slow(new_x, y, max(HARD_LIMIT_LOW+5000, min(HARD_LIMIT_HIGH-5000, new_z)),   0); 
	digitalWrite(LED_BUILTIN, HIGH); digitalWrite(LED_BUILTIN, LOW); 
	//if (adcpin > ADC_THRESHOLD) {	digitalWrite(LED_BUILTIN, LOW); } else { digitalWrite(LED_BUILTIN, HIGH); }; 
  }


  // 10 kHz main loop cycle; todo: should use timer interrupt instead
  ets_delay_us(100); 
}

/* 
Future TODOs in firmware:

Nice to have:
*/ 



  //Xout += Xslope;
  //if ((Xout==0) || (Xout==255)) Xslope=-Xslope;
  //if (Xout==0) {
    //Yout += Yslope;
    //if ((Yout==0) || (Yout==255)) Yslope=-Yslope;
  //}
  //dacWrite(25, Xout);  
  //dacWrite(26, Yout);

// https://openlabpro.com/guide/timer-on-esp32/
