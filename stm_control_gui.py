#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""
A simple graphical user interface to the scanning tunneling microscope

Module dependencies: numpy, serial, struct, tkinter
    sudo pip install gwyfile

    TODO
    * avoid storing z position in global var here (duplicit)
    * try compiling+uploading with gcc & esp-idf instead of bloated arduino studio
"""

import sys, os, time, datetime, serial, subprocess, struct
import matplotlib, matplotlib.pyplot, matplotlib.backend_bases
#matplotlib.use('TkAgg')
try:
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
except:
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
    from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg as NavigationToolbar2Tk

import numpy as np
import tkinter as tk
from tkinter import messagebox


## Hardware-specific settings
#XMIN,XMAX,XSPEED,XSTEP = -4000, 4000, 1, 50 ## TODO
#YSTEP,YRES = 50, 160

XMIN,XMAX,XSPEED,XSTEP = -4000, 4000, 1, 10 ## TODO
YSTEP,YRES = 10, 160*5
#Note: in my mechanical setup, 38293 microsteps are one mm
def um2steps(um): return int(38.293*256 * um)
## == error handling with a graphical message box == 
#def myerr(exc_type, exc_value, tb): 
    #import traceback 
    #message = '\r'.join(traceback.format_exception(exc_type, exc_value, tb))
    #print(message); messagebox.showerror(title=exc_value, message=message)
#sys.excepthook = myerr

## == settings loading == {{{
def load_settings_from_file(infile='settings.txt'):
    settings = {}
    with open(infile) as f:
        for l in f.readlines():
            l = l.split('#')[0] # ignore comments
            if not l.strip(): continue
            k,v = [s.strip() for s in l.split('=', 1)]  # split at first '=' sign
            settings[k] = v
    return settings
settings = load_settings_from_file()# }}}

## == Serial port init == 
## On Linux one needs first to disable the "hangup" signal, to prevent ESP32 randomly resetting. 
## A command-line solution is: stty -F /dev/ttyUSB0 -hup
## Note that none of the serial module attributes (like rtscts, dsrdtr, xonxoff, setDTR(), setRTS()) could solve this
try:
    import termios
    with open(settings['serialport']) as f: # 
        attrs = termios.tcgetattr(f)
        attrs[2] = attrs[2] & ~termios.HUPCL
        termios.tcsetattr(f, termios.TCSAFLUSH, attrs)
except ImportError:
    pass        # termios is not available on Windows, but probably also not needed

# Standard serial port settings, as hard-coded in the hardware
port = serial.Serial(port=settings['serialport'], baudrate=921600, bytesize=serial.EIGHTBITS, 
        parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout= 0.1)
if not port.isOpen(): port.open()


## == Hardware control routines == 
CMD_STEPPERM =  1
CMD_APPROACH =  2
CMD_GET_STEPPER_STATUS =  3
CMD_GET_STM_STATUS =  4
CMD_SET_PIEZO =  9
CMD_LINESCAN = 10

PIEZO_X = 0
PIEZO_Y = 1
PIEZO_Z = 2

ZTABLE_MOTOR_ID = 0
MINIMUM_POS = -2**31+1

z = 0 


def stepperm(relative_targetpos, motor_speed, motor_id = ZTABLE_MOTOR_ID):
    """ Universal low-level stepper motor control: sets the new target position of the selected
    stepper motor, along with the maximum speed to be used. """
    #setpiezo(0,0,-1000000)   ## retract piezo to minimum limit as a safety measure
    global z
    z = get_stepper_status(motor_id=ZTABLE_MOTOR_ID)['nanopos']   # sync to the STM's motor pos 
    port.write(struct.pack(r'<BBii', CMD_STEPPERM, motor_id, z+int(relative_targetpos), int(np.ceil(motor_speed))))
    print('stepper:', relative_targetpos, z, motor_speed)
    z += relative_targetpos
def get_stepper_status(motor_id = ZTABLE_MOTOR_ID):       
    """ Universal low-level stepper motor control: returns a byte whether the motor is running, 
    another whether it is on end switch and an integer of the motor's current nanoposition. """
    port.write(struct.pack(r'<BB', CMD_GET_STEPPER_STATUS, motor_id))
    return dict(zip(['active', 'endswitch', 'nanopos'], struct.unpack(r'<BBi', port.read(6))))
def get_stm_status():
    port.write(struct.pack(r'<B', CMD_GET_STM_STATUS))
    status = dict(zip(['approach_active', 'piezo_feedback_active', 'piezo_x', 'piezo_y', 'piezo_z', 'tip_voltage', 'stm_data_len'], 
        struct.unpack(r'<BBhhhhh', port.read(12))))
    status['stm_data'] = struct.unpack(r'<{:d}h'.format(status['stm_data_len']//2), port.read(status['stm_data_len']))
    return status
def approach(relative_targetpos, motor_speed, motor_cycle_steps, piezo_speed):
    global z; 
    z = get_stepper_status(motor_id=ZTABLE_MOTOR_ID)['nanopos']   # sync to the STM's motor pos 
    values = struct.pack(r'<BBiiih', CMD_APPROACH, ZTABLE_MOTOR_ID, z+relative_targetpos, motor_speed, motor_cycle_steps, piezo_speed)  
    #values = struct.pack(r'<BBiiih', CMD_APPROACH, ZTABLE_MOTOR_ID, motor_targetpos, motor_speed, motor_cycle_steps, piezo_speed)   ##XXX
    port.write(values)
    while True:
        time.sleep(.2) ## FIXME
        stepper_status = get_stepper_status(); print(time.time(), stepper_status)
        stm_status = get_stm_status()
        #; print(stm_status)
        #print('done.')
        if not stm_status['approach_active']: break
    print('STM approach finished')
    z = stepper_status['nanopos']
        
    # todo should cycle here until the "engaged" z-position is known (and set the z variable)


def setpiezo(x,y,z): # for DEBUGGING
    values = struct.pack(r'<Bhhh', CMD_SET_PIEZO, int(x), int(y), int(z))   #int(2**15 / 2.9 * 1.3 / 1.343 * ) ... for volts at DAC
    port.write(values)

def linescan(y): 
    # scan_x_start scan_x_end scan_x_speed scan_x_sampling_ste scan_y_position 
    #port.write(struct.pack(r'<Bhhhhh', CMD_LINESCAN, -2000,2000,1,10,int(y))) ## XXX
    port.write(struct.pack(r'<Bhhhhh', CMD_LINESCAN, XMIN,XMAX,XSPEED,XSTEP,int(y))) ## XXX
    l[0] = (l[0]+1) % t.shape[0]
    l[1] = 0
    print(time.time(), 'init linescan number', l[0], 'at y=', y)

## == GUI init == 
## --- build the main window ---
root = tk.Tk()
root.geometry("900x850+0+0")
root.pack_propagate(0) # don't shrink
root.wm_title("STM control")
root.protocol("WM_DELETE_WINDOW", lambda: (root.destroy(), root.quit()))  # correct termination upon window close (also on windows)


## --- build the plotting window ---
fig, ax  = matplotlib.pyplot.subplots(1, figsize=(3, 4), dpi=90)
## --- initialize plotted data ---
l = [-1, 0]                          ## coordinate of the currently scanned pixel
t = np.nan * (np.zeros([YRES,80]))
#im  = ax.imshow(t, interpolation='nearest', aspect=.25)  # 'bicubic' would crop pixels surrounded by one or more NaNs!
im  = ax.pcolorfast(t, interpolation='nearest', aspect=.25)  # 'bicubic' would crop pixels surrounded by one or more NaNs!
cb  = fig.colorbar(im)


canvas = FigureCanvasTkAgg(fig, master=root)
canvas.draw()
toolbar = NavigationToolbar2Tk(canvas, root)
toolbar.update()
canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

stm_status = {}

def timer(): 
    """ Periodic updating of the graphical user interface """
    root.after(int(settings['update_timer_ms']), timer) # schedule the next call of this routine
    global stm_status
    stm_status = get_stm_status()
    root.wm_title('V={:1.3f} (x,y,z)={:+04d},{:+04d},{:+04d}'.format(
        stm_status['tip_voltage']/4096*3.3,stm_status['piezo_x'],stm_status['piezo_y'],stm_status['piezo_z']) +
        (' AA' if stm_status['approach_active'] else '   ') +
        (' FB' if stm_status['piezo_feedback_active']&0x01 else '   ') +
        (' SC' if stm_status['piezo_feedback_active']&0x02 else '   ') +
        (' RX' if stm_status['stm_data'] else '   '))
    for zd in stm_status['stm_data']:
        global t
        while l[1] >= t.shape[1]: 
            t = np.pad(t, [[0,0],[0,1]], mode='constant', constant_values=np.nan)  
        t[l[0], l[1]] = zd / float(settings['um2piezo_z'])
        l[1]+=1
    if stm_status['stm_data']:
        im.set_clim((np.min(t[t>-np.inf]), np.max(t[t>-np.inf])))
        im.set_data(t)
        fig.canvas.draw()

    # if a line was just finished (and not at the end of image), automatically continue scanning a new line
    if stm_status['stm_data'] and not stm_status['piezo_feedback_active']&0x02 and (l[0]+1)<t.shape[0]: linescan((l[0]-t.shape[0]//2)*YSTEP)  ## yscan XXX
timer()

def save_stm_data(name):
    from gwyfile.objects import GwyContainer, GwyDataField, GwySIUnit
    obj = GwyContainer()
    print('SAVING X EXTENT', t.shape[1]*XSTEP / float(settings['um2piezo_xy']) / 1e6)
    print('SAVING Y EXTENT', t.shape[0]*YSTEP / float(settings['um2piezo_xy']) / 1e6)
    print('SAVING t[0]', t[0,:10],'...',t[0:-10:])
    print('SAVING t[1]', t[1,:10],'...',t[1:-10:])
    print('SAVING t[-1]', t[-1,:10],'...',t[-1:-10:])
    obj['/0/data'] = GwyDataField(
            data=t/1e6, 
            xreal = t.shape[1]*XSTEP / float(settings['um2piezo_xy']) / 1e6,
            yreal = t.shape[0]*YSTEP / float(settings['um2piezo_xy']) / 1e6,
            si_unit_xy = GwySIUnit(unitstr='m'),
            si_unit_z = GwySIUnit(unitstr='m')
            )
    print('saving to '+ name +'.gwy')
    obj.tofile(name+'.gwy')
def run_ext_editor(name):
    subprocess.Popen([*(settings['external_editor'].split()), name])
def on_key_event(event):
    if event.key == 'ctrl+J': # K go up, J go down
        print('you pressed %s' % event.key) # debug
    if event.key in ('s', 'S'):
        name = datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S_') + settings['samplename']
        save_stm_data(name)
        #fig.savefig(name+'.png') # PNG export not needed
        print('saving') # debug
        if event.key == 'S': run_ext_editor(name+'.gwy')
        return
    matplotlib.backend_bases.key_press_handler(event, canvas, toolbar)
root.bind('<Escape>', lambda e: (root.destroy(), quit()))
canvas.mpl_connect('key_press_event', on_key_event)

class StepperButton(tk.Button):
    def __init__(self, updown, power, **kwargs):
        self.updown, self.power = updown, power
        kwargs['text'] = 'z {:} {:g} μm'.format('↟ closer' if updown>0 else '↡ retract', 1000/10**i)
        kwargs['command'] = lambda: stepperm(relative_targetpos=um2steps(1000./10.**self.power)*updown, motor_speed=int(1*256./(2**power)))
        kwargs['bg'] = '#ee8888' if updown > 0 else '#8899ee'
        tk.Button.__init__(self, **kwargs)

frame1 = tk.Frame(master=root).pack(side=tk.LEFT)
frame2 = tk.Frame(master=root).pack(side=tk.RIGHT)
for i in range(0,5): StepperButton(+1, i, master=frame1).pack(side=tk.TOP) 
tk.Button(master=root, text='↥ z safe approach (200 μm up)', bg='#88ee88', 
        command=lambda: approach(relative_targetpos=um2steps(200), motor_speed=16, motor_cycle_steps=um2steps(.5), piezo_speed=10)).pack(side=tk.TOP)

def init_scan():
    l = [-1, 0]                          ## coordinate of the currently scanned pixel (note that line number l[0] will be incremented before 1st px is received)
    setpiezo(XMIN,(l[0]-t.shape[0]//2)*YSTEP,0)
    print('init scan', l)
tk.Button(master=root, text='⇱ init position', bg='#88ee88', command=lambda: init_scan()).pack(side=tk.TOP)
tk.Button(master=root, text='↯ STM start scanning', bg='#88ee88', command=lambda: linescan((l[0]-t.shape[0]//2)*YSTEP)).pack(side=tk.TOP)
for i in range(4,-1,-1): StepperButton(-1, i, master=frame2).pack(side=tk.TOP) 


tk.mainloop()

#from mpl_toolkits import mplot3d
#ax = matplotlib.pyplot.axes(projection='3d'); 
#X, Y = np.meshgrid(np.linspace(0,1,t.shape[1]), np.linspace(0,1,t.shape[0]))
#im = ax.plot_surface(X, Y, t, rstride=1, cstride=1, cmap='winter', edgecolor='none')  #TODO

#colours = ['red','green','orange','white','yellow','blue']
#r = 0
#for c in colours:
    #tk.Label(text=c, relief=tk.RIDGE, width=15).grid(row=r,column=0)
    #tk.Entry(bg=c, relief=tk.SUNKEN, width=10).grid(row=r,column=1)
    #r = r + 1

# todo: make it like thorlabs_apt on github:
    #>>> import thorlabs_apt as apt
    #>>> apt.list_available_devices()
    #[(50, 55000038)]
    #>>> motor = apt.Motor(55000038)
    #>>> motor.move_home(True)
    #>>> motor.move_by(45)
