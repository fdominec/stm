  // DAC control pins
  //pinMode(DAC_LATCHEN, OUTPUT);
  //digitalWrite(DAC_CLOCK, HIGH); 
  //pinMode(DAC_CLOCK, OUTPUT);   // clock is common for all DACs
  //pinMode(DAC_DATA0, OUTPUT);
  //pinMode(DAC_DATA1, OUTPUT);
  //pinMode(DAC_DATA2, OUTPUT);
  //pinMode(DAC_DATA3, OUTPUT);
  //for (uint8_t i=0; m<DAC_NUMBER; m++) {  pinMode(dac_data[m], OUTPUT); } // TODO







void my_delay(uint32_t del) {
	//for (uint32_t counter=0; counter<del; counter++) {
	    //digitalWrite(34, HIGH);	
		//digitalWrite(34, LOW);	
	//}
}



void set_piezo_PCM56P(int32_t new_x, int32_t new_y, int32_t new_z) {
// This routine is no more recommended since PCM56P is somewhat bulky and requires also the -5 V power supply.
// But we keep it ready in case of bi-polar 16-bit DAC were needed in the future.
// Don't forget that the HARD_LIMIT_LOW and HARD_LIMIT_HIGH would change, too:
//#define HARD_LIMIT_LOW  0				
//#define HARD_LIMIT_HIGH (1<<(DACBITS-1))	
//#define SOFT_LIMIT_LOW  -HARD_LIMIT_HIGH/6+1			// prevents dac underflow  // todo
//#define SOFT_LIMIT_HIGH HARD_LIMIT_HIGH/6-1			// prevents dac overflow, given for a bipolar dac by (1<<dacbits)/2/3 since x,y,z are added

	uint32_t dac_word0        = (HARD_LIMIT_LOW+HARD_LIMIT_HIGH)/2 + x + y + z;  
	uint32_t dac_word1        = (HARD_LIMIT_LOW+HARD_LIMIT_HIGH)/2 - x + y + z;  
	uint32_t dac_word2        = (HARD_LIMIT_LOW+HARD_LIMIT_HIGH)/2 - x - y + z;  
	uint32_t dac_word3        = (HARD_LIMIT_LOW+HARD_LIMIT_HIGH)/2 + x - y + z;  

	digitalWrite(DAC_LATCHEN, HIGH);	
	ets_delay_us(1); 
	for (uint8_t bit=0; bit<(DACBITS+1); bit++) {
		digitalWrite(DAC_CLOCK, LOW);
		ets_delay_us(1); // simple serial communication with four little-endian DACs at once
		if (dac_word0 & (1<<(DACBITS-1))) {digitalWrite(DAC_DATA0, HIGH);} else {digitalWrite(DAC_DATA0, LOW);};  dac_word0 = dac_word0 << 1; 
		if (dac_word1 & (1<<(DACBITS-1))) {digitalWrite(DAC_DATA1, HIGH);} else {digitalWrite(DAC_DATA1, LOW);};  dac_word1 = dac_word1 << 1;
		if (dac_word2 & (1<<(DACBITS-1))) {digitalWrite(DAC_DATA2, HIGH);} else {digitalWrite(DAC_DATA2, LOW);};  dac_word2 = dac_word2 << 1;
		if (dac_word3 & (1<<(DACBITS-1))) {digitalWrite(DAC_DATA3, HIGH);} else {digitalWrite(DAC_DATA3, LOW);};  dac_word3 = dac_word3 << 1;
		ets_delay_us(1); 
		digitalWrite(DAC_CLOCK, HIGH);
		ets_delay_us(1); 
	}
	digitalWrite(DAC_LATCHEN, LOW); ets_delay_us(1); 
	digitalWrite(DAC_CLOCK, LOW);   ets_delay_us(1); 
	digitalWrite(DAC_CLOCK, HIGH);  
}

