// ********************************************************************************
#define i2s_num 0
int i2s_write_sample_nb(uint32_t sample){
  return i2s_write_bytes((i2s_port_t)i2s_num, (const char *)&sample, sizeof(uint32_t), 100);
}




















//i2s configuration 
i2s_config_t i2s_config = {
     .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX),
     .sample_rate = 36000,
     .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
     .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
     .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
     .dma_buf_count = 8,
     .dma_buf_len = 64   //Interrupt level 1
    };
    
i2s_pin_config_t pin_config = {
    .bck_io_num = NDAC_BCK, //this is BCK pin
    .ws_io_num = NDAC_WS, // this is LRCK pin
    .data_out_num = NDAC_DATA01, // this is DATA output pin
    .data_in_num = -1   //Not used
};
  int32_t q = 0;
       //initialize i2s with configurations above
        i2s_driver_install((i2s_port_t)i2s_num, &i2s_config, 0, NULL);
        i2s_set_pin((i2s_port_t)i2s_num, &pin_config);
        //set sample rates of i2s to sample rate of wav file
        i2s_set_sample_rates((i2s_port_t)i2s_num, 10000); 
  while (1) {  // ---------------------------------
	//i2s_write_sample_nb(((q&255)<<8)+(q&(255*256))>>8);
	//i2s_write_sample_nb(q);
	i2s_write_sample_nb(q>>8);
#define tempstep 1
	if (q >= (256*256-tempstep)) {
		q=0*256;
	    digitalWrite(LED_BUILTIN, HIGH);	
		ets_delay_us(13250);
		digitalWrite(LED_BUILTIN, LOW);	
		} else {q += tempstep; }; 
	};
// ################################################################################
